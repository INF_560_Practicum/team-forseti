# Team Forseti Repository
The repo has files that invovle data preprocessing, data cleaning and modeling and dashboard.
```
+-- Analysis
+-- Model
+-- Preprocessing
+-- Report
+-- Deck

```
### Preprocessing
This folder contains notebooks that perform data cleaning nad preprocessing 
```
+-- Preprocessing
|	+-- Data Preprocessing.ipynb
|	+-- Data_Driver.py
|	+-- Data_Loading.ipynb
|	+-- PArse_Files.ipynb
```


### Analysis
This folder contains notebooks that perform rule based classification by analysing distributions for passengers, staff and devices. 
```
+-- Analysis
|	+-- Classifying Data  Passengers or Airport Staff Employee or Devices.ipynb
```



### Model
This folder contains notebooks that perform modelling using Decicison Tree, XGBoost, Random Forest, NN, CatBoost etc
```
+-- Model
|	+-- Modeling - All Flyers.ipynb
|	+-- Terminal 1.ipynb
```
### Deck & Report
Contain the presentation and explanantion of the entire implementation of the project.
