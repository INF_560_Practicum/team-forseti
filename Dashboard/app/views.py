from app import app
from flask import Flask, render_template
import DBManipulate as DB


@app.route('/')
@app.route('/index.html')
def dashBoard():

    data = {
        "T1": {
            "eating": DB.getCountByBehavior(1, 0),
            "shopping": DB.getCountByBehavior(1, 1),
            "waiting": DB.getCountByBehavior(1, 2),
        },
        "T2": {
            "eating": DB.getCountByBehavior(2, 0),
            "shopping": DB.getCountByBehavior(2, 1),
            "waiting": DB.getCountByBehavior(2, 2),
        },
        # "pr": {
        #     "eating": 451,
        #     "shopping": 216,
        #     "waiting": 294,
        # }
    };
    data['eating'] = data["T1"]["eating"] + data["T2"]["eating"]
    data['shopping'] = data['T1']['shopping'] + data['T2']['shopping']
    data['waiting'] = data['T1']['waiting'] + data['T2']['waiting']
    return render_template('index.html', data=data)


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('testTemplate.html')


@app.route('/detail/<terminal>/<time>')
def detail(terminal=1, time="_"):
    week = DB.getCurrentWeek()
    hour = DB.getNextHourPeriod()
    category="3";
    if time != "_":
        week = time.split("_")[0]
        hour = time.split("_")[1]
        category = time.split("_")[2]
    data = {};

    def getLoc(jsonList):
        result = []
        for i in jsonList:
            loc = {
                'lat': i['lat'],
                'lng': i['lng']
            }
            result.append(loc)
        return result
    if terminal == "T1":
        data = {"eating": DB.getCountByBehavior(1, 0, week, hour, category), "shopping": DB.getCountByBehavior(1, 1, week, hour, category),
                "waiting": DB.getCountByBehavior(1, 2, week, hour, category), 'terminal': "Terminal1", "dataSet": {
                'eating': getLoc(DB.getListByBehavior(1, 0, week, hour, category)),
                "shopping": getLoc(DB.getListByBehavior(1, 1, week, hour, category)),
                "waiting": getLoc(DB.getListByBehavior(1, 2, week, hour, category))
            }};
    elif terminal == "T2":
        data = {"eating": DB.getCountByBehavior(2, 0, week, hour, category), "shopping": DB.getCountByBehavior(2, 1, week, hour, category),
                "waiting": DB.getCountByBehavior(2, 2, week, hour, category), 'terminal': "Terminal2", "dataSet": {
                'eating': getLoc(DB.getListByBehavior(2, 0, week, hour, category)),
                "shopping": getLoc(DB.getListByBehavior(2, 1, week, hour, category)),
                "waiting": getLoc(DB.getListByBehavior(2, 2, week, hour, category))
            }};

    elif terminal == "pr":
        data = {"eating": 44, "shopping": 2, "waiting": 29, 'terminal': "Piersul"};

    data['week']=week;
    data['hour']=hour;
    data['category']=category;



    return render_template('/Terminal.html', data=data)
