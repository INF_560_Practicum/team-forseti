import datetime
import os
import pymongo
import json
import datetime
from bson.json_util import dumps


client = pymongo.MongoClient(host='35.188.151.199', port=27017)
db = client.kianaData
collection = db.model
def getCurrentWeek():
    return datetime.datetime.now().weekday()

def getCurrentHourPeriod():
    return int(datetime.datetime.now().hour/2)+1

def getNextHourPeriod():
    return (getCurrentHourPeriod())%12+1

def getTotalNum():
    return collection.find().count()

def getCountByBehavior(terminal,behavior,week=getCurrentWeek(),time=getNextHourPeriod(),category="3"):
    param={'terminal':terminal,'behavior':str(behavior),'week':str(week),'time':str(time)}
    print(param)
    if not category=="3" :
        param["flyCat"]=str(category)
    result= collection.count_documents(param)
    return result

def getListByBehavior(terminal,behavior,week=getCurrentWeek(),time=getNextHourPeriod(),category="3"):
    param = {'terminal': terminal, 'behavior': str(behavior), 'week': str(week), 'time': str(time)}
    if not category == "3":
        param["flyCat"] = category
    result = collection.find(param)
    return result


