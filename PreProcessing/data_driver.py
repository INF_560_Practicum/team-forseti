import json
for i in range(100,212):
	f = open("../Data/tps2_0-10/tps2000000000"+str(i),"r")
	print('tps2000000000'+str(i))
	contents = f.readlines()
	print(len(contents))
	import csv
	for content in contents:
		data = json.loads(content)
		Building = data['Building']
		Level = data['Level']
		file_name = 'rio_bq_'+Building+'_'+Level+'.csv'
		with open('../CSV/'+file_name,mode='a') as data_writer_file:
			data_writer = csv.writer(data_writer_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			data_writer.writerow([data['Building'], data['Level'], data['ClientMacAddr'], data['lat'], data['lng'], data['localtime']])